const Agenda = require('agenda');
const queue = require('./../lib/kue');

const mongoConnectionString = 'mongodb://127.0.0.1/agenda';

const agenda = new Agenda({db: {address: mongoConnectionString}});


agenda.define('queueSyncs', (job, done) => {
  queue.create('sync', {}).save(function (error) {
    if (error) {
      return done(error);
    }
    return done();
  });
});

(async function() {
  await agenda.start();

  await agenda.every('1 minutes', 'queueSyncs');
})();