const rp = require('request-promise');

const access_token = "af2d3dd1fac937e95bc13396d993634b";

async function getVariants() {
  const options = {
    method: "GET",
    url: 'https://fightmilk-node.myshopify.com/admin/products.json',
    headers: {
      'X-Shopify-Access-Token': access_token
    },
    json: true
  };

  const response = await rp(options);

  return response.products.flatMap(product => (product.variants));
}

module.exports = { getVariants };