const VariantModel = require("./../Models/Variant");
const Hapi = require('@hapi/hapi');
const queue = require("./../lib/kue");

require('./../scheduler/scheduler');

const server = Hapi.Server({
  port: 3000,
  host: 'localhost'
});

server.route({
  method: "GET",
  path: "/",
  handler: () => "Hi-diddly-ho neighbor-eeno!"
});
server.route({
  method: "GET",
  path: "/sync",
  handler: async (request, h) =>
    queue.create('sync', {}).save(error => {
      if (error) return h.response(error).code(500);
      return h.response();
    })
});
server.route({
  method: 'GET',
  path: '/variants',
  handler: async (request, h) => {
    try {
      const variants = await VariantModel.find();
      return h.response(variants);
    }
    catch (error) {
      return h.response(error).code(500);
    }
  }
});
server.route({
  method: "GET",
  path: "/variant/{id}",
  handler: async (request, h) => {
    try {
      const variant = await VariantModel.findById(request.params.id);
      return h.response(variant);
    }
    catch (error) {
      return h.response(error).code(404);
    }
  }
});

exports.init = async () => {

  await server.initialize();
  return server;
};

exports.start = async () => {

  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
  return server;
};

process.on('unhandledRejection', (err) => {

  console.log(err);
  process.exit(1);
});

