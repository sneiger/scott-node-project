const Promise = require('bluebird');
const { getVariants } = require("../Channels/Shopify");
const VariantModel = require('../Models/Variant');

class Sync {
  static async sync() {
    const connectorProducts = await Sync.getConnectorProducts();
    const formattedProducts = Sync.getFormattedProducts(connectorProducts);
    const formattedProductsForCreateAndUpdate = await Sync.getFormattedProductsForCreateAndUpdate(
      formattedProducts);

    await Sync.createFormattedProducts(formattedProductsForCreateAndUpdate.createProducts);
    await Sync.updateFormattedProducts(formattedProductsForCreateAndUpdate.updateableProducts);
  }

  static async getConnectorProducts() {
    return getVariants();
  }

  static getFormattedProducts(connectorProducts) {
    return connectorProducts.map(connectorProduct => ({
      channelId: connectorProduct.id,
      parentId: connectorProduct.product_id,
      title: connectorProduct.title,
      sku: connectorProduct.sku,
      stock: connectorProduct.inventory_quantity
    }));
  }

  static async getFormattedProductsForCreateAndUpdate(formattedProducts) {
    const createProducts = [];
    const updateableProducts = [];
    await Promise.mapSeries(formattedProducts, async (formattedProduct) => {
      const savedProduct = await VariantModel.findOne({ 'channelId': formattedProduct.channelId });
      if (savedProduct) {
        const updatedProduct = Object.assign(formattedProduct, {
          '_id': savedProduct._id
        });
        updateableProducts.push(updatedProduct)
      } else {
        createProducts.push(formattedProduct)
      }
    });

    return {
      createProducts,
      updateableProducts
    };
  }

  static async updateFormattedProducts(updateableProducts) {
    return Promise.mapSeries(updateableProducts, async (updateableProduct) => {
      return VariantModel.findOneAndUpdate({
        '_id': updateableProduct._id
      }, updateableProduct)
    })
  }

  static async createFormattedProducts(createProducts) {
    return Promise.mapSeries(createProducts, async (createProduct) => {
      const variantModel = new VariantModel(createProduct);
      return variantModel.save();
    });
  }
}

module.exports = Sync;