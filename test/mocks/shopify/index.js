const fs   = require('fs');
const path = require('path');

module.exports = {
  getProduct: fs.readFileSync(path.join(__dirname, 'get-product.json')),
  listProducts: fs.readFileSync(path.join(__dirname, 'list-products.json')),
  listProductsWithUpdates: fs.readFileSync(path.join(__dirname, 'list-products-with-updates.json')),
  listProductsWithUpdatesAndNewProducts: fs.readFileSync(path.join(__dirname, 'list-products-with-new-products-and-updates.json')),

};