const Lab = require('@hapi/lab');
const nock = require('nock');
const { expect } = require('@hapi/code');
const ShopifyMocks = require('./../mocks/shopify');
const Sync = require('./../../lib/VariantProcessor');
const VariantModel = require('./../../Models/Variant');

require('./../../lib/database-test');

const lab = exports.lab = Lab.script();

const {
  describe,
  it,
  afterEach,
  beforeEach,
} = lab;

nock.disableNetConnect();

beforeEach(async () => {
  await VariantModel.remove({})
});

afterEach(async () => {
  nock.cleanAll();
});

describe('Inventory Sync Test', () => {
  it('should create database variants from shopify variants', async () => {
    // should get variants from shopify
    const shopifyListProductsRequest = nock('https://fightmilk-node.myshopify.com/admin')
    .get('/products.json')
    .reply(200, ShopifyMocks.listProducts);

    await Sync.sync();

    expect(shopifyListProductsRequest.isDone()).to.be.true();

    const variants = await VariantModel.find({});

    expect(variants.length).to.equal(1);
    expect(variants[0].parentId).to.equal(3798735716426);
  });


  it('should update database variants for existing db variant', async () => {
    // should get variants from shopify
    let shopifyListProductsRequest = nock('https://fightmilk-node.myshopify.com/admin')
    .get('/products.json')
    .reply(200, ShopifyMocks.listProducts);

    await Sync.sync();

    expect(shopifyListProductsRequest.isDone()).to.be.true();

    let variants = await VariantModel.find({});

    expect(variants.length).to.equal(1);
    expect(variants[0].parentId).to.equal(3798735716426);
    expect(variants[0].stock).to.equal(5);
    expect(variants[0].sku).to.equal("fightmilk-og");

    // should get variants from shopify
    shopifyListProductsRequest = nock('https://fightmilk-node.myshopify.com/admin')
    .get('/products.json')
    .reply(200, ShopifyMocks.listProductsWithUpdates);

    await Sync.sync();

    expect(shopifyListProductsRequest.isDone()).to.be.true();

    variants = await VariantModel.find({});

    expect(variants.length).to.equal(1);
    expect(variants[0].parentId).to.equal(3798735716426);
    expect(variants[0].stock).to.equal(20);
    expect(variants[0].sku).to.equal("changedmyshit");
  });


  it('should update database variants for existing db variant and save new varaints', async () => {
    // should get variants from shopify
    let shopifyListProductsRequest = nock('https://fightmilk-node.myshopify.com/admin')
    .get('/products.json')
    .reply(200, ShopifyMocks.listProducts);

    await Sync.sync();

    expect(shopifyListProductsRequest.isDone()).to.be.true();

    let variants = await VariantModel.find({});

    expect(variants.length).to.equal(1);
    expect(variants[0].parentId).to.equal(3798735716426);
    expect(variants[0].stock).to.equal(5);
    expect(variants[0].sku).to.equal("fightmilk-og");

    // should get variants from shopify
    shopifyListProductsRequest = nock('https://fightmilk-node.myshopify.com/admin')
    .get('/products.json')
    .reply(200, ShopifyMocks.listProductsWithUpdatesAndNewProducts);

    await Sync.sync();

    expect(shopifyListProductsRequest.isDone()).to.be.true();

    variants = await VariantModel.find({});

    expect(variants.length).to.equal(2);
    expect(variants[0].parentId).to.equal(3798735716426);
    expect(variants[0].stock).to.equal(20);
    expect(variants[0].sku).to.equal("changedmyshit");
    expect(variants[1].parentId).to.equal(3798735716429);
    expect(variants[1].stock).to.equal(100);
    expect(variants[1].sku).to.equal("newVariant");
  })
});