const mongoose = require("mongoose");

const { Schema } = mongoose;

const variantSchema = new Schema({
  channelId: Number,
  parentId: Number,
  title: String,
  sku: String,
  stock: Number,
  source: String
}, {
  timestamps: true
});

const VariantModel = mongoose.model('Variant', variantSchema);

module.exports = VariantModel;