const Queue = require('./../lib/kue');
const Sync = require('./../lib/VariantProcessor');

require ('./../lib/database');


async function sync() {
    return Sync.sync();
}

Queue.process('sync', (job, done) => (
  sync(job)
  .then(done)
  .catch(function (error) {
    console.log(error);
    return done(error);
  })
));